calligra (1:24.12.3-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 04 Mar 2025 20:05:13 +0000

calligra (1:24.12.2-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 04 Feb 2025 20:05:10 +0000

calligra (1:24.12.1-0neon) noble; urgency=medium

  [ Jonathan Riddell ]
  * New release with KDE Gear

  [ Neon CI ]
  * New release

 -- Neon CI <neon@kde.org>  Tue, 07 Jan 2025 20:05:10 +0000

calligra (1:4.0.1-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 03 Sep 2024 20:05:12 +0000

calligra (1:4.0.0-0neon) jammy; urgency=medium

  * Applied upstream. Remove ki18n patch. 

 -- Carlos De Maine <carlosdemaine@gmail.com>  Mon, 16 Jan 2023 17:35:26 +1000

calligra (1:3.2.1+dfsg-5neon3) jammy; urgency=medium

  * Applied upstream. Remove ki18n patch. 

 -- Scarlett Moore <sgmoore@debian.org>  Thu, 06 Oct 2022 09:30:46 -0700

calligra (1:3.2.1+dfsg-5neon2) jammy; urgency=medium

  * Attempt to add patch to add ki18n install po in cmake. 

 -- Scarlett Moore <sgmoore@debian.org>  Thu, 06 Oct 2022 08:13:13 -0700

calligra (1:3.2.1+dfsg-5neon1) jammy; urgency=medium

  * Update python recommends to python3 as python2 is no more. 

 -- Scarlett Moore <sgmoore@debian.org>  Wed, 05 Oct 2022 06:43:40 -0700

calligra (1:3.2.1+dfsg-5neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 02 Feb 2018 14:28:24 +0000

calligra (1:3.0.1-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 04 Apr 2017 10:17:32 +0000

calligra (1:3.0.0.1-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 04 Jan 2017 10:41:09 +0000

calligra (1:2.9.11+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Create the symlink in calligra-gemini only when the package is built;
    fix arch-indep builds.

 -- Pino Toscano <pino@debian.org>  Mon, 10 Oct 2016 23:32:39 +0200

calligra (1:2.9.11+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Update Vcs-* fields.
  * Backport upstream commit 843c41decfa85e351349f7a410893ac85c9d60b7 to find
    PostgreSQL 9.6; patch upstream_cmake-find-PostgreSQL-9.6.patch.
    (Closes: #840071)
  * Make calligra-gemini start: (Closes: #840243)
    - create a symlink in the non-multi-arch library directory to the
      multi-arch installation place, as the latter option is not handled by
      kdelibs 4.x
    - add the libqtwebkit-qmlwebkitplugin dependency, as the QtWebKit module
      is used

 -- Pino Toscano <pino@debian.org>  Mon, 10 Oct 2016 21:04:44 +0200

calligra (1:2.9.11+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Drop from calligra-data the two mimetype definitions msooxml-all.xml, and
    x-iwork-keynote-sffkey.xml, since shared-mime-info provides them already.
  * Remove unused calligra-map-shape.install.
  * Backport upstream commit 04d61e92515f19cbd2dbb0cc06d4340ba4514a8a to fix
    build on armel/armhf; patch upstream_fix-arm-FTBFS.patch.
  * Backport upstream commit 956bb80db4f300e4f8edeaf19d24fd61eb1932b2 to fix
    build on platforms where char is unsigned; patch
    upstream_Fix-compilation-of-PsCommentLexer.cpp-on-platforms-w.patch.
  * Update the patches:
    - add_keywords_to_desktop_files.patch: refresh
  * Update the build dependencies:
    - remove libglew-dev, and shared-desktop-ontologies: unused
    - add libxml2-dev, indirectly needed by the pkg-config files of
      libetonyek, and libvisio (it was not an issue, as the non-pkg-config
      way was then used)
  * Remove obsolete parts in README.source.

 -- Pino Toscano <pino@debian.org>  Sat, 17 Sep 2016 11:05:45 +0200

calligra (1:2.9.11+dfsg-1) unstable; urgency=low

  * Team upload.
  * New upstream release. (Closes: #787033)
  * The upload (with rebuild) should make all the binaries installable again.
    (Closes: #806454)

  [ Dmitry Smirnov ]
  * Added "debian/TODO.Debian".
  * Patchworks:
    - eigen3.patch
    - eigen3_in_tests
    - install_images_in_datadir
    - kisjpegtest_call_kisexiv2_initialize
    - switch-to-librevenge-based-import-libs.patch
    + no-planconvert-jar.patch
    * patches refreshed, sorted and renamed consistently ending with ".patch".
  * control:
    - dropped unnecessary versioned Build-Deps.
    - dropped "krita-sketch" package.
    + updated Vcs-Browser URL.
    + Standards-Version: 3.9.6.
    + turned "krita-gemini" into transitional package.
  * Build-Depends:
    + libboost-system-dev
    + postgresql-server-dev-9.4
    + libgit2-dev
  * rules: explicitly select QT4 to fix FTBFS.
  * calligra-libs.install:
    + liblibglobal.so.1*
    + liblibkispsd.so.1*
  * calligrawords.install:
    + libkoversion.so.1*
  * krita.install:
    + libkritacolor.so.1*
  * calligraplan: do not install "PlanConvert.jar" + patch.
  * Gemini:
    + Use fonts from "texlive-fonts-extra" instead of bundled ones.
    + Split Gemini arch-indep files into -data package.
  * Updated "package-name-doesnt-match-sonames" lintian-overrides.
  * Added "no-symbols-control-file" lintian-overrides.
  * Converted "debian/prune-nonfree" target to "copyright/Files-Excluded";
    Added "repacksuffix=+dfsg" to "debian/watch".
  * Copyright: complete rewrite.
  * Files-Excluded:
    + 3rdparty/google-breakpad
    + filters/libmso/generated/mso.jar
    + filters/plan/mpxj/planconvert/jar/PlanConvert.jar
  * watch: updated and corrected watch file.
  * Lintianisation:
    - xs-testsuite-header-in-debian-control

  [ Raúl Sánchez Siles ]
  * Refresh xbase64.diff patch
  * Refresh switch-to-librevenge-based-import-libs.patch

  [ Pino Toscano ]
  * watch file: ignore +dfsg suffix in Debian version number.
  * Exclude more stuff with the repack:
    - "windows" and "winquirks" subdirectories, as containing only material
      for Windows
  * Drop krita entirely from this source, since a newer version of it is going
    to be provided by a separate krita source:
    - add the whole "krita" subdirectory to the Excluded-Files, so it is
      removed from the repacked tarball (making it a lot smaller)
    - pass -DBUILD_krita=OFF to cmake, to make sure it is not used
    - drop the krita, and krita-data binaries
    - drop the removal of krita_ora.xml, since it is no more installed
    - drop paragraph about Vc in README.source
    - comment out the krita dependency in the calligra metapackage for now
    - drop the local krita man page
    - drop krita bits from copyright
    - update the patches accordingly:
      - add_keywords_to_desktop_files.patch: drop krita bit
      - cmake-set_kritasketch_lib_soversion.patch: drop, no more needed
      - filter_registry_not_a_singleton.patch: drop, no more needed
      - imports_search_path.patch: drop, no more needed
  * Drop old leftovers of krita-sketch package.
  * Update Vcs-* fields.
  * Drop unneeded build dependencies:
    - libexiv2-dev, libfftw3-dev, libkdcraw-dev, libopencolorio-dev,
      libopenjpeg-dev, libtiff-dev, and libxi-dev: used for krita only
      (Closes: #826808)
    - libkactivities-dev: the kde4 version is not useful anymore
    - libmarble-dev: it is Qt5 only now
      - drop kexi-map-form-widget and calligra-reports-map-element, since they
        are no more built (Closes: #818395)
    - nepomuk-core-dev: Nepomuk was phased out and removed already
  * Replace the libmysqlclient-dev with default-libmysqlclient-dev, so the
    default MySql implementation in Debian is used.
  * Replace deprecated libgsl0-dev build dependency with libgsl-dev.
  * Replace the postgresql-server-dev-9.4 build dependency with
    postgresql-server-dev-all: while the latter will install more packages,
    it also installs the -server-dev package of the default postgresql version
    in Debian.
  * Unconditionally remove the development stuff, since there is not going to
    be any -dev package at this point.
  * Remove calligra-dbg in favour of the -dbgsym packages.
  * Drop autopkgtests test suite run: a whole Calligra rebuild and run of the
    test suite is too long and heavy for the Debian CI, and generally not
    what autopkgtests is for. (Closes: #785653)
  * Update install files.
  * Stop using the sodeps dh addon, since there are no more -dev packages.
  * Update lintian overrides.
  * Replace all the khelpcenter4 suggests with khelpcenter.
  * Replace the libqca2-plugin-ossl recommend with libqca2-plugins.
  * Remove patch cmake-do_not_install_removed_files.patch, already not
    applied anymore.
  * Bump Standards-Version to 3.9.8, no changes required.
  * Cleanups to copyright.
  * Tighten dependencies on calligra-libs, and kexi.

  [ Maximiliano Curia ]
  * Update homepage links. (Closes: #762305) Thanks to Julian Gilbey

  [ Adrien Grellier ]
  * new build dependency: libetonyek
  * install files has been forgotten
  * new binary packages:
     - okular-backend-odt
     - calligra-gemini

  [ Scott Kitterman ]
  * Add libsoprano-dev to build-depends since it is no longer transitively
    pulled in. (Closes: #797389)

 -- Pino Toscano <pino@debian.org>  Mon, 12 Sep 2016 06:22:49 +0200

calligra (1:2.8.5+dfsg-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * oops, actually bump libwps build-dep to >= 0.4...

 -- Rene Engelhard <rene@debian.org>  Sun, 21 Jun 2015 08:38:24 +0200

calligra (1:2.8.5+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * build against libwps 0.4.0 (closes: #788969)
  * add missing libetonyek-dev build-dependency

 -- Rene Engelhard <rene@debian.org>  Tue, 16 Jun 2015 23:10:07 +0200

calligra (1:2.8.5+dfsg-1) unstable; urgency=medium

  * New upstream release. (Closes: #744291, #748990)

  [ Adrien Grellier ]
  * new dependency for krita: libxi-dev
  * refresh add_keywords_to_desktop_files.patch to work with 2.8
  * refresh eigen3.patch to work with 2.8
  * new package: calligra-semanticitems
  * update to Standards-Version 3.9.5 (no changes)
  * update copyright information for beta3
  * patch to build using xbase64, thanks Matthias Klose! (Closes: #746227)
  * move html2ods.so from calligrawords to calligrasheets, thanks shadeslayer!
  * update copyright information for calligra 2.8.5

  [ Pino Toscano ]
  * Add the pkg-config build dependency.
  * Update lintian overrides.
  * Remove the uninstalled man page of visualimagecompare, since that tool
    is not installed either.
  * Update install files.

  [ Raúl Sánchez Siles ]
  * Add libodfgen-dev and libopencolorio-dev build dependencies.
  * Update install files.
  * Update sonames in lintian-overrides.
  * Add calligra-semanticitems depends.
  * Adding lintian-overrides for krita-sketch.
  * Enable kpresenter to odp filter.

  [ Maximiliano Curia ]
  * Remove obsolete patch: pqxx_4_support.diff
  * Update build dependencies.
  * Add autopkgtests.
  * Refresh patches
  * New patch: eigen3_in_tests
  * New patch: disable_convolution_failling_tests
  * New patch: temporarily_disable_failing_tests
  * Update copyright file.
  * Re-enable krita-sketch and krita-gemini.
  * New patch: calligra_imports_search_path
  * New patch: set_kritasketch_lib_soversion
  * New patch: install_images_in_datadir
  * Bump libkdcraw and marble b-d, to help the kde4.12 transition.
  * Refresh patches for 2.8.5.
  * New patch: kisjpegtest_call_kisexiv2_initialize
  * New patch: filter_registry_not_a_singleton
  * New patch: channelFlags_logic_change
  * New patch: do_not_install_removed_files
  * Update install files for the repack version.
  * Add hspell dependency to the tests.
  * Update debian/copyright generation instructions, added a static part
    in copyright.tail for the things missed by lcheck.

  [ Rene Engelhard ]
  * Add patch to build against the new librevenge-based import libs and update
    build-dependencies. (Closes: #754568)

 -- Maximiliano Curia <maxy@debian.org>  Mon, 11 Aug 2014 16:49:01 +0200

calligra (1:2.7.5-1) unstable; urgency=low

  * New upstream release.
    - A new component appears: calligraactive
    - New import filter for calligraplan, based on Java MPXJ.
      See /usr/share/doc/calligraplan/README.Debian for using it.

  [ Adrien Grellier ]
  * update install files for calligra 2.7.1
  * update copyright file for calligra 2.7.1
  * build with libkactivites, as the version 6.1 is now in sid
  * build with libphononexperimental
  * update to my new official email address
  * split kexi to kexi-data for arch-indep files
  * patch to add keywords to desktop files
  * update lintian overrides

  [ Pino Toscano ]
  * Update .install files.
  * Specify the required version for libkactivities-dev.
  * Update watch file to look for .xz extension too.

  [ Maximiliano Curia ]
  * Migrate from eigen2 to eigen3, thanks to Anton Gladky. (Closes: #726643)
  * Add pqxx 4 support. (Closes: #730632)
  * Refresh patches.
  * Add myself to uploaders.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 03 Dec 2013 18:45:27 +0100

calligra (1:2.6.4-1) unstable; urgency=low

  * New upstream release.
  * Team upload.

  [ Pino Toscano ]
  * Add calligraauthor to the calligra metapackage. (Closes: #710832)
  * Remove unused stuff of kthesaurus and calligramobile.
  * Remove koconverter man page, no such tool anymore.
  * Manually remove from debian/tmp all the things that are known to not be
    installed anywhere; this shortens the output of dh_install --list-missing.
  * Properly install the calligraconverter and kexi_sqlite3_dump man pages.
  * Add the nepomuk-core-dev build dependency, needed with KDE >= 4.9/4.10.
    (Closes: #717413) This effectively raises the minimum requirements to
    KDE 4.10.

  [ Adrien Grellier ]
  * Update installed files.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 20 Jul 2013 13:59:37 -0300

calligra (1:2.6.3-2) unstable; urgency=low

  [ Adrien Grellier ]
  * calligrastage must depend on calligrastage-data
  * calligrastage-data: add a lintian override for
    desktop-command-not-in-package for calligrastage

  [ Pino Toscano ]
  * Make calligra-libs break/replace kexi < 1:2.6.1, and raise the
    calligra-data breaks/replaces wrt kexi to the same version (related to
    #704597).
  * Manually disable the unit tests; they are not enabled by default until
    kdelibs 4.9, while since 4.10 they are.
  * Update lintian overrides.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Team upload.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 29 May 2013 10:49:24 -0300

calligra (1:2.6.3-1) unstable; urgency=low

  [ Raúl Sánchez Siles ]
  * New upstream release.
  * fix lintian override for libkdgantt11 and libkowv2_9

  [ Adrien Grellier ]
  * Release Calligra 2.6.3 to unstable.
  * Make calligra-data Break/Replace kexi < 1:2.4 (Closes: #704597)
  * Add a manpage for calligraauthor, as requested by lintian
  * Add a manpage for calligraconverter
  * Add a manpage for kexi_sqlite3_dump
  * Add a Readme.source to document the Debian packaging
  * update to Standards-Version 3.9.4 (no changes)
  * create calligrastage-data, for arch-independant files
    (lintian: arch-dep-package-has-big-usr-share 7564kB 63%)
  * use canonical URI for VCS-* fields (lintian: vcs-field-not-canonical)
  * inherit « Section: kde » in the control file
    (lintian: binary-control-field-duplicates-source)
  * Copyright: removes lintian warning by adding the licenses paragraph
    missing

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Make calligra-libs recommend libqca2-plugin-ossl for encryption
    (Closes: #705567).

 -- Adrien Grellier <adrien.grellier@laposte.net>  Fri, 24 May 2013 20:52:47 +0200

calligra (1:2.6.1-1) experimental; urgency=low

  [ Raúl Sánchez Siles ]
  * New upstream release
    - Removed do_not_link_blas.diff. Included upstream.
    - calligramobile and kthesaurus are removed
    - A new component appears : calligraauthor
    - Build Dep: Add libglib2.0-dev for kexi mdb migration
    - Build Dep: switch from libwmf to libvectorimage

 -- Adrien Grellier <adrien.grellier@laposte.net>  Sun, 03 Mar 2013 10:32:54 +0100

calligra (1:2.4.4-2) unstable; urgency=low

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Team upload.

  [ Adrien Grellier ]
  * Make calligrasheets break/replace koffice-data < 1:2.4. (Closes: #700667)

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 20 Feb 2013 14:48:41 -0300

calligra (1:2.4.4-1) unstable; urgency=low

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Team upload.
  * Fix the previous changelog:
    - krita is the package that breaks/replaces krita-data.
    - Add the missing epochs.
    The changes were correctly done in the control file, the changes were just
    typos in the changelog. Thanks Thorsten Glaser for noticing.

  [ Adrien Grellier ]
  * Fix typo in calligrasheets package description (Closes: #697261)
  * Make calligrastage break/replace koffice-data < 1:2.4 (Closes: #699200)
  * New upstream bug fix release 2.4.4:
    General:
      - Update some translations
    Sheets:
      - A very nasty bug which made recalculations of spreadsheet
        contents go wrong under some circumstances (BUGS: 312981, 313010)
    Kexi:
      - Make palette background color property in text editor work (BUG: 309274)
      - Fix “data source tag” for text box (regression because of changes in
        KDE libraries’ line editor)
      - SQLite Driver: Fix possible data loss of compacted file (when process
        tools crash or fail for any reason)
      - Remove limit of 101 fields in Kexi Table Designer (BUG: 309116)
      - Fix MySql login failure when not saving password – fix for
        case 3.1 (BUG: 313025)
      - Fix possible crash when pressing tab in the global search box
  * Remove upstream_Make-sure-not-to-write-behind-the-allocated-memory.patch
    included upstream

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 09 Feb 2013 11:42:46 -0300

calligra (1:2.4.3-4) unstable; urgency=low

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Team upload.

  [ Pino Toscano ]
  * Make krita break/replace krita-data < 1:2.4. (Closes: #694854)
  * Make karbon break/replace koffice-data < 1:2.4. (Closes: #694855)

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 15 Dec 2012 21:19:38 -0300

calligra (1:2.4.3-3) unstable; urgency=low

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Team upload.

  [ Pino Toscano ]
  * Make calligrawords-data conflict/replace old koffice-data.
    (Closes: #694397)

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 26 Nov 2012 18:34:10 -0300

calligra (1:2.4.3-2) unstable; urgency=low

  * Team upload.

  [ Adrien Grellier ]
  * Fix karbon: extra space for paragraph separation in the package
    description. (Closes: #679731)
  * Split the templates from calligra-data to the application's packages.
    (Closes: #682763)

  [ Pino Toscano ]
  * Backport upstream commit 7d72f7dd8d28d18c59a08a7d43bd4e0654043103 to fix
    a buffer overflow in the msword import filter (CVE-2012-3456); patch
    upstream_Make-sure-not-to-write-behind-the-allocated-memory.patch.
    (Closes: #684004)
  * Force the "kde" build system to dh_auto_configure, so the proper kdeinit
    handling is applied.
  * Fix description of krita to be within 80 columns.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sun, 19 Aug 2012 15:06:46 -0300

calligra (1:2.4.3-1) unstable; urgency=low

  * Team upload.

  [ Raúl Sánchez Siles ]
  * New upstream release
    - Removed patch compile_on_arm.diff, included upstream.

  [ Eshat Cakar ]
  * Improve package descriptions, thanks to Justin B Rye (Closes: #679230)

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Fri, 29 Jun 2012 14:32:59 +0200

calligra (1:2.4.2-3) unstable; urgency=low

  * Team upload

  [ Pino Toscano ]
  * Fix conflict/replaces with old koffice names. (Closes: #677085)
  * Make okular-backend-odp enhance okular.

  [ Sune Vuorela ]
  * Ensure we are using the new libmarble-dev (this can be relaxed later if
    needed)

 -- Sune Vuorela <sune@debian.org>  Fri, 22 Jun 2012 08:56:20 +0200

calligra (1:2.4.2-2) unstable; urgency=low

  * Pull a patch from upstream to make calligra build on arm (Closes: #676576)

 -- Sune Vuorela <sune@debian.org>  Mon, 04 Jun 2012 08:53:30 +0200

calligra (1:2.4.2-1) unstable; urgency=low

  [ Pino Toscano ]
  * New upstream release.
  * Update install files.
  * Update lintian overrides.
  * Convert copyright to copyright-format v1.0, and make it more compliant.

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Wed, 30 May 2012 19:10:10 +0200

calligra (1:2.4.1-1) unstable; urgency=low

  * Team upload.

  [ Adrien Grellier ]
  [ Raúl Sánchez Siles ]
  [ Pino Toscano ]
  * Initial release. (Closes: #665903)

 -- Pino Toscano <pino@debian.org>  Sat, 26 May 2012 13:39:21 +0200
